# Netværk- og kommunikationssikkerhed

## Læringsmål
**Viden**

*Den studerende har viden om og forståelse for:*

- Netværkstrusler
- Trådløs sikkerhed
- Sikkerhed i TCP/IP
- Adressering i de forskellige lag
- Dybdegående kendskab til flere af de mest anvendte internet protokoller (ssl)
- Hvilke enheder, der anvender hvilke protokoller
- Forskellige sniffing strategier og teknikker
- Netværk management (overvågning/logning, snmp)
- Forskellige VPN setups
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)

**Færdigheder**

*Den studerende kan:*

- Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot)
- Teste netværk for angreb rettet mod de mest anvendte protokoller
- Identificere sårbarheder som et netværk kan have.

**Kompetencer**

*Den studerende kan håndtere udviklingsorienterede situationer herunder:*

- Designe, konstruere og implementere samt teste et sikkert netværk
- Monitorere og administrere et netværks komponenter
- Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report)
- Opsætte og konfigurere et IDS eller IPS

## Eksamen

**ECTS:**

Prøvens omfang er 10 ECTS.

**Prøveform:**

Prøven er en individuel, mundtlig prøve med udgangspunkt i et spørgsmål, som den studerende trækker til eksamen.

Alle spørgsmål, der kan trækkes til eksamen, er udleveret til de studerende senest 14
dage før eksamen, så de studerende har mulighed for at forberede sig. 

Hvad angår
spørgsmålene, vil der blive lagt vægt på, at den studerende kan inddrage eksempler fra
projektarbejdet og praktiske øvelser fra det forgangne semester. 

Der er ingen forberedelse på selve dagen.

Den individuelle, mundtlige eksamen varer 25 minutter inkl. votering.

**Bedømmelsen:**

Prøven bedømmes efter 7-trinsskalaen med ekstern bedømmelse.

**Prøven er ikke bestået:**

Prøveformen er den samme ved reeksamen
