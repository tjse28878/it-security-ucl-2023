# **Linux Cheat sheet**

Denne dokumentation fokuserer på observationer, der er gjort under primære opgaver inden for system sikkerhed, med særlig vægtning af kommandoer til Linux. Formålet er at give en mere præcis og målrettet tilgang til materialer, der kan hjælpe dem, der arbejder med system sikkerhed.

Linux er et populært open source-operativsystem, som anvendes både i personlige og professionelle miljøer. Der findes utallige kommandoer, der kan anvendes i Linux, og det kan ofte være en udfordring at finde frem til de relevante kommandoer. I denne dokumentation vil vi fokusere på de vigtigste kommandoer inden for system sikkerhed og deres praktiske anvendelse.

Udover kommandoer vil dokumentationen også dække andre emner inden for system sikkerhed såsom netværkssikkerhed, databasesikkerhed og overvågning. Disse emner vil blive dækket med henblik på at give et overblik over de vigtigste principper og bedste praksis inden for system sikkerhed.

Alt i alt vil denne dokumentation give en praktisk tilgang til system sikkerhed med fokus på de væsentlige emner, som professionelle i branchen har brug for at kende til.

##Filestrukturen i linux

Her er nogle notater om de forskellige directories i Linux filestrukturen:

`/bin`: Dette directory indeholder binære filer (executables) til systemets basisfunktioner. Disse filer er nødvendige for at kunne starte og køre Linux, og de bruges ofte i startprocessen.

`/boot`: Dette directory indeholder de filer, der er nødvendige for at starte operativsystemet. Det inkluderer bl.a. bootloaderen og kernel-filerne.

`/dev`: Dette directory indeholder filer, der repræsenterer hardwareenheder på computeren. Disse filer bruges til at kommunikere med og styre hardwaren.

`/etc`: Dette directory indeholder systemkonfigurationsfiler, som styrer indstillinger for systemet og dets applikationer.

`/home`: Dette directory indeholder en undermappe til hver bruger på systemet, hvor de kan gemme deres personlige filer og indstillinger.

`/media`: Dette directory indeholder mount points til midlertidige filsystemer, som f.eks. eksterne USB-drev eller cd'er.

`/lib`: Dette directory indeholder libraries, som er samlinger af kode, der bruges af programmer til at udføre specifikke opgaver.

`/mnt`: Dette directory indeholder mount points til permanente filsystemer, som f.eks. netværksdrev eller NFS-filer.

`/misc`: Dette directory indeholder mount points til midlertidige filsystemer, som ikke hører til nogen specifik kategori.

`/proc`: Dette directory indeholder filsystemet, der bruges til at tilgå systeminformation og -statistikker i realtid`.

`/opt`: Dette directory indeholder ekstra softwarepakker, der ikke er en del af det primære operativsystem.

`/sbin`: Dette directory indeholder systembinærer, der bruges til at administrere systemet.

`/root`: Dette directory er hjemmemappen til superbrugeren (root).

`/tmp`: Dette directory indeholder midlertidige filer, som slettes, når computeren genstartes.

`/usr`: Dette directory indeholder mange af de ikke-systemrelaterede programmer og libraries, som er installeret på computeren.

`/var`: Dette directory indeholder variable datafiler, som f.eks. systemlogfiler og mail-spools.

Disse directories følger en standardstruktur, som gør det lettere for brugere og systemadministratorer at navigere og forstå filestrukturen i Linux. 

##pwd (print working directory)
Print Working Directory (PWD) er en kommando i Linux, som viser den aktuelle arbejdsmappe, hvor du befinder dig i systemet. Det er en nyttig kommando, da den giver dig en oversigt over den nuværende mappe, som kan være nyttig i forhold til at navigere i systemet.

Når du åbner en terminal i Linux, vil den vise dig den nuværende arbejdsmappe. Hvis du vil se den samme information i en anden mappe, kan du bruge PWD-kommandoen til at få vist den aktuelle mappe i terminalen.

Her er et Eksemple på brugen af PWD-kommandoen:

For at vise den aktuelle mappe i terminalen, skal du blot skrive __pwd__ og trykke på __Enter__.


##cd (change directory)

"cd" er en kommando i Linux, der står for "change directory". Kommandoen bruges til at skifte den aktuelle arbejdsmappe til en anden mappe i filsystemet.

Når du åbner en terminal i Linux, er din aktuelle arbejdsmappe normalt din hjemmemappe. Ved at bruge "cd" kan du navigere til forskellige mapper på filsystemet og udføre opgaver eller kommandoer i disse mapper.

**Her er et par eksempler på brugen af "cd" kommandoen i Linux:**

flyt et directory frem :

```
cd 'næste directory' 
```
 
 flyt et directory tilbage:
 
```cd .. ```

flyt til root directory:

```cd /```

flyt til home directory:


`cd ~`


##ls (List)

"ls" er en kommando i Linux, der viser en liste over filer og mapper i den aktuelle arbejdsmappe eller en bestemt mappe i filsystemet.
 Kommandoen er ofte brugt i terminalen til at navigere og undersøge filsystemet.
 
 **Her er et par eksempler på brugen af "ls" kommandoen i Linux:**
 
For at vise en liste over filer og mapper i den aktuelle mappe:
```
ls' 
 ```
 
Hvis du vil se en liste over filer og mapper i en bestemt mappe:
 
```ls /sti/til/mappe ```

vis skjulte filer og mapper:

```ls -a```

vis størrelser i en mere læsbar form:

`ls -h`

kombinere indstillingerne(for at få en mere detaljeret liste over alle filer og mapper, inklusive skjulte filer.):

`ls -lha`


 
 

##touch
"touch" er en kommando i Linux, der opretter en tom fil i den aktuelle arbejdsmappe eller en bestemt mappe i filsystemet,
 hvis filen ikke allerede eksisterer. Hvis filen allerede eksisterer, opdaterer "touch" kommandoen datoen og 
 klokkeslættet for sidste ændring til den aktuelle dato og tidspunkt.
 
 
**Eksempler på brug**

For at oprette en ny fil i den aktuelle mappe, kan du skrive touch filnavn og trykke på Enter.
 Dette opretter en ny tom fil med det angivne navn i den aktuelle mappe. Hvis filen allerede eksisterer,
 vil datoen og klokkeslættet for sidste ændring blive opdateret.
Hvis du vil oprette en ny fil i en bestemt mappe, kan du skrive:

`touch /sti/til/mappe/filnavn` og trykke på Enter.


 Dette opretter en ny tom fil med det angivne navn i den angivne mappe.
 
 
Hvis du vil oprette flere filer på én gang, kan du skrive `touch fil1 fil2 fil3` og trykke på Enter. 

Dette opretter tre nye tomme. filer med de angivne navne i den aktuelle mappe. 




##mkdir 

"mkdir" er en kommando i Linux, der opretter en ny mappe i den aktuelle arbejdsmappe eller en bestemt mappe i filsystemet, hvis mappen ikke allerede eksisterer.
Eksempler på brug:

Opret en ny mappe med det angivne navn i den aktuelle mappe.

`mkdir mappe_navn` 

Opret en ny mappe med det angivne navn i den angivne mappe.

`mkdir /sti/til/mappe/ny_mappe`

Opret flere undermapper på én gang i den angivne sti.

`mkdir -p sti/til/ny/mappe`

Opret flere mapper på én gang i den aktuelle mappe.


`mkdir mappe1 mappe2 mappe3` 

Bemærk: "mkdir" kræver normalt skriveadgang til den pågældende mappe, du forsøger at oprette. 
Hvis du ikke har de nødvendige tilladelser, vil kommandoen mislykkes med en fejlmeddelelse.

dette afhjælpes ved at burge komandoen `sudo` forand de overnævnte komandoer

