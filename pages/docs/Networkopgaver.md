# Opgave 1 - Fagets læringsmål

## Information
I denne opgave skal i undersøge fagets læringsmål.

Formålet er at sikre at i ved:

- hvor læringsmålene kan findes
- hvad de betyder
- hvad i kan bruge dem til
- og ikke mindst, bliver det lettere at huske læringsmålene hvis i har arbejdet lidt med dem.

## Instruktioner
1. Find læringsmålene i studieordningen.
2. Diskuter i teamet hvordan i forestiller jer at læringsmålene er relevante for faget og hvad vi kan bruge dem til. 

___

**Besvarelse**

- *Mange af læringsmålene indbefatter meget basal teknisk viden, som understøtter vores læring af mere højtstående teknologier.*
- *Nogle af læringsmålene hjælper en med at vurdere hvilken tilgang man skal have i de specifikke problemer. Hvilken indgangsvinkel man kan starte med.*
- *Nemmere at bygge systemer med sikkerhed i mente fra start, end at skulle implementere sikkerhed i et allerede eksisterende system.*

Hvad skal vi bruge det til?

- Skabe forståelse for, hvad vi skal kunne når faget er ovre.
- Hjælpe med at forventningsafstemme med skolen/underviserne/os selv.
- Hjælpe med at kunne komme igennem eksamen.
- Hjælpe med at have et grundlag hvorfra vi kan skabe videre viden efterfølgende.

___

# Opgave 2 - OSI Model

## Information
OSI model har 7 lag.

## Instruktioner
1. Find almindelige (consumer) eksempler på hardware enheder, og placer dem i OSI modellen.
2. Find nye/sjældne/esoteriske enheder (fysiske eller virtuelle) (med referencer) - f.eks. next-gen firewalls, proxies, application gateways. Hvad som helst med et netstik/wifi er ok at have med.
3. Forbered jer på diskussion/præsentation af hvad I har fundet så vi kan dele viden på klassen.

___

**Besvarelse**

### Del 1

- Tastatur - Applikationslag
- Router - Netværkslag
- Grafikkort - Præsentationslag
- Harddisk - Datalinklaget
- Mikrofon - Fysisk lag.
- Webkamera - Fysisk lag: 
- Trådløst netværksadapter - Datalinklag:
- Bluetooth dongle - Datalinklag: 
- USB flashdrev - Fysisk lag: 
- Monitor - Præsentationslag

### Del 2 


**The latest ML-Powered NGFWs, including new fourth generation hardware**
De seneste ML-powered NGFW'er, inklusive ny fjerde generations hardware Sikre alle lokationer, fra det mindste kontor til de største datacentre i verden og alt derimellem, med verdens mest avancerede ML-powered NGFW. Få fuldstændig synlighed og kontrol over applikationer på tværs af alle brugere og enheder
 – hvor som helst og når som helst.
 
 
*reference*
>[Understand the power of PA-Series NGFWs](https://www.paloaltonetworks.com/network-security/next-generation-firewall-hardware)

# Opgave 3 - Fejlfinding

## Information
Dette er en øvelse i fejlfinding, og formålet er at få testet og belyst din tankegang når du arbejder struktureret. 

Tag et problem du har i løbet af ugen, og gennemarbejd det. 

## Instruktioner

1. Vælg et problem, gerne når det opstår, alternativt på bagkant. Det kunne være printerfejl, wifi connection issues, hjemmesider der er ‘væk’, langsomt internet, certifikat fejl, mv.
2. Beskriv problemet. Vær opmærksom på forskellen mellem hvad der er observeret, og hvad der er “rygmarvsreaktion”.
3. Beskriv hvad du tror problemet er (ie. formuler en hypotese)
4. Find oplysninger der underbygger/afkræfter ideen (ie. se i logfiler, lav tests, check lysdioder, …) og skriv det ned
5. Hvis ideen blev afkræftet, lav en ny hypotese.
6. Beslut hvad der kan gøres for at afhjælpe eller mitigere fejlen, hvis den skulle komme igen.
7. Implementer det hvis ressourcerne tillader det, eller beskriv hvordan det skal håndteres til næste gang.

**Bonus spørgsmål**
Hvordan håndterede du at nå grænsen for din viden og/eller forståelse af problemet eller et del-element?

___

**Besvarelse**

1. Jeg har et Problem når jeg forbinder min telefon til min bil via. USB 
2. Problemet er at jeg prøver at oprette forbindelse med usb men den automatiske forbindelse Bluetooth vinder forbindelsesretighederne.
3. Den automatiske forbindelse Bluetooth vinder forbindelsesretighederne.
4. Jeg fandt ikke andre der have samme problem som mig da jeg undersøge internetet. men i det jeg slog bluetooth fra have jeg forbindelse med det samme.
5.  *hypotese blev ikke afkræftet
6. 	- Jeg undersøgte internette om jeg kunne lave en preferance til forbindelsenern. men uden held. 
	- Jeg opdateret min telefon.
	___
	- **Problem løst.**
	___
7. **ALTID OPDATER DIT UDSTYR**

...

___


# Opgave 4 - specs

gave 4 - specs

##Information
Vi vil se på specs for forskelligt hardware, og sammenligne.

## Instruktioner

Find listen fra ww06, hvor lavede vi en liste over hardware og deres plads i OSI modellen.
Udvælg 2-3 forskellige enheder fra listen
Find specifikationerne for enheden
Hvad bliver listet som begrænsninger? Synes du det er høje tal? lave tal?


Links
Som eksempel, datablad for [Junipe SRXes](https://www.juniper.net/us/en/products/security/srx-series/srx300-line-services-gateways-branch-datasheet.html)

___

**Besvarelse**

Jeg har installeret __[Icotera i5851-00](https://www.nab.dk/media/11601/fibia_installationsguide_icotera_i5851_med-tv-boks_web_apr18.pdf) __ 
 fibernetboksen derhjemme, og i denne opgave vil jeg beskrive dens specifikationer. 
Jeg har fundet oplysningerne om enheden på [Icotera Hjemmeside](https://icotera.com/media/1465/icotera-ftth-cpe-i-series-product-catalogue-v50.pdf) og ved at undersøge dens
 konfigurationsmuligheder, som vist på vedlagte skærmbillede af "Configuration possibilities".

__[Icotera i5851-00](https://www.nab.dk/media/11601/fibia_installationsguide_icotera_i5851_med-tv-boks_web_apr18.pdf) __ fibernetbox er hvad jeg har monteret hjemme hos mig selv. 


![1/4](images/Opgave4-specs(1).png)


cotera i5850 fiber gateway er en højtydende, multi-funktionel enhed, der kombinerer avancerede funktioner med kraftfulde hardware og software. 
Enheden er bygget på en dual-core arkitektur og inkluderer en ASIC til pakke-forwarding, så den er i stand til at håndtere yderligere opgaver, 
mens den behandler VoIP, Gigabit routing, traffic switching/bridging og højhastigheds Wi-Fi.

I5850 giver fremragende Wi-Fi-funktionalitet med understøttelse af den nyeste 802.11ac wave 2-standard, der leverer hastigheder på 1700+300 Mbps 
og gennemstrømning på mere end 1 Gbps i virkelige miljøer. Enheden tilbyder også backwards-kompatibilitet med enhver 802.11a/b/g/n Wi-Fi certificeret enhed.

I5850 giver også exceptionel Layer 2-funktionalitet med evnen til at håndtere 16 bridging-instanser, flere WAN-grænseflader, PPPoE og in-band sikker 
administration. Enheden inkluderer en CATV AGC-modtager og understøtter en tilpasningsbar filterløsning med low-pass filtre til individuelle RF-kanalplaner.

Enheden kan nemt styres og administreres gennem en række protokoller som OMCI v2, SNMP v1/v2, syslog, SSH, Telnet og TR-069 og inkluderer også en fail-proof,
 zero-touch auto provisioning-mekanisme til problemfri daglige operationer. Enheden understøtter også dual-bank firmware til nemme firmware-udruller i krævende netværksmiljøer.

I5850 er integreret med Icotera Smart Home-platformen, der tilbyder gode løsninger for sluttbrugere i områder som Alarm & Overvågning, Energiadministration 
og Hjemmeautomatisering. Den cloud-baserede platform forbinder til tredjeparts-hardware, der tilbyder ubegrænsede muligheder for at tilslutte enheder og giver
 netværksoperatører og serviceudbydere yderligere indtægtsstrømme og højere kundeloyalitet.
 
***Forkortelser:***


__VoIP:__ Voice over Internet Protocol (Stemme over Internettet-protokol)
VoIP står for Voice over Internet Protocol og refererer til en teknologi, der gør
 det muligt at foretage telefonsamtaler over internettet i stedet for gennem traditionelle telefonledninger. Dette sparer penge på telefonregninger og gør det nemmere for folk at kommunikere over store afstande.

__ASIC__ Application-Specific Integrated Circuit (Applikationsspecifik integreret kredsløb)
ASIC står for Application-Specific Integrated Circuit og refererer til en elektronisk enhed,
 der er designet til at udføre en bestemt opgave. De er ofte brugt i it-udstyr, da de er hurtigere og mere energieffektive end generelle formål kredsløb.

__Wi-Fi:__ Wireless Fidelity (Trådløs nøjagtighed)
Wi-Fi står for Wireless Fidelity og refererer til en teknologi, der gør det muligt at oprette
 en trådløs forbindelse til internettet eller en lokal netværksenhed. Dette gør det nemt for folk 
 at få adgang til internettet uden at skulle tænde for en kabelforbindelse.

__WAN:__ Wide Area Network - WAN står for Wide Area Network og refererer til et stort computernetværk, der dækker en stor geografisk område,
 fx et helt land eller endda hele verden. Dette gør det nemt for folk at kommunikere og dele data på tværs af store afstande.

__PPPoE:__ Point-to-Point Protocol over Ethernet (Point-til-point protokol over Ethernet) - Dette er en standard, der anvendes til at oprette en sikker og 
pålidelig forbindelse til internettet gennem en bredbåndsforbindelse, såsom DSL eller kablet internet.

__CATV__ AGC receiver__: Cable Television Automatic Gain Control Receiver (Kabel-tv automatisk forstærkningskontrol-modtager) - Dette er en enhed, der bruges til at modtage tv-signaler fra en kabel-tv-udbyder og regulere signalstyrken, så billedkvaliteten forbliver stabil.

__OMCI v2 :__ OMCI version 2  - Dette er en standard for at styre og administrere broadband-udstyr, der bruges af internetudbydere og -operatører.

__SNMP v1/v2: __  Simple Network Management Protocol version 1 og 2 (Enkel netværksstyring-protokol version 1 og 2) - Dette er en standard, der bruges til at overvåge og administrere netværksudstyr og -enheder fra fjernsteder.

__Syslog:__ System Log - Dette er en teknologi, der bruges til at logge information om en enheds aktiviteter og fejl til en central server for analyse og fejlfinding.

__SSH:__ Secure Shell - Dette er en sikker netværksprotokol, der bruges til at få adgang til en enheds terminal og administrere den.

__Telnet:__ Telecommunication Network (Telekommunikationsnetværk) - Dette er en standard, der bruges til at oprette en forbindelse til en enheds terminal fra en fjernplacering.

__TR-069:__ Technical Report 069- Dette er en standard, der bruges til at administrere og opdatere bredbåndsudstyr og -enheder.




___


#Opgave 5 - internet speeds

##Information
Der er flere services på nettet der tilbyder at teste båndbredde

##Instruktioner

Gå på [speedtest](https://speedtest.net/)

Hvad er hastigheden?
Best guess: hvad er bottleneck, og hvorfor.

Prøv evt. at køre den samtidigt på to maskiner på samme subnet/wifi.

___

**Besvarelse**

###hastighedstest på UCL's netværk og hjemme###

Formålet med denne opgave er at undersøge og sammenligne internetforbindelsernes hastighed og pålidelighed
 på UCL's netværk og derhjemme. Jeg har udført en hastighedstest på begge forbindelser
 og undersøgt forskellige faktorer, der kan påvirke forbindelsens hastighed.
 
 ___UCL's netværk___

Jeg har taget en hastighedstest på UCL's netværk ved hjælp af Speedtest. Resultaterne af testen kan ses i nedenstående billede.

![2/4](images/speedtest.png)

Som det ses på billedet, var downloadhastigheden ca. 62 Mbps og uploadhastigheden var 77 Mbps. Jeg konkluderede, at hastigheden var begrænset
 af reglerne på skolens routerindstillinger.

___ Hjemmenetværk___


Jeg udførte den samme hastighedstest, da jeg kom hjem til min fibernetforbindelse på 1000/1000 Mbps. Jeg havde dog ikke forventninger
 om at opnå samme hastighed som på UCL's netværk på grund af to faktorer:
 
![3/4](images/speedtest hjemme.png)

- WiFi-forbindelse: Jeg bruger WiFi-tilslutning derhjemme, som kan forårsage datatab og reducere forbindelseshastigheden til 800/800 Mbps.
- Afstand og hindringer: Signalet skal passere gennem to rum, hvilket kan forårsage yderligere nedsættelser. Jeg forventede en forbindelse på 675/500 Mbps.

Resultaterne af testen viste, at hastigheden var langt under mine forventninger. Jeg konkluderede, at mine forventninger til min hjemmeforbindelse og dens pålidelighed var to forskellige ting.

For at undersøge årsagen til den lave hastighed, testede jeg forbindelsen på andre enheder.
Resultaterne af testene på min mobiltelefon var ikke meget bedre med en forbindelse på __153/142 Mbps.__ Testen af min arbejdscomputer
gav lidt bedre resultater, men hastigheden var stadig langt under forventet med __192/131 Mbps.__

Jeg vil fortsætte med at undersøge grunden til den lave hastighed på min forbindelse og overveje alternativer, 
så jeg kan få en mere pålidelig og hurtigere internetforbindelse derhjemme.

**konklusion**

jeg skal kontakte stofa om hvorfor forbindelsen ikke lever op til deres mininmum krav på 800/800mbps



___

#Opgave 6 - more internet speed

##Information
Iperf er et værktøj til den slags.

##Instruktioner

Installér iperf
Kør Iperf mod en public server, e.g. iperf3 -c <some server> -t 10 -i 1 -p <some port>

Kør Iperf mod en public server (modsat retning), e.g. iperf3 -c <some server> -t 10 -i 1 -p <some port> -R

Sammenlign resultat. Var hastigheder som forventet?

Note: Der er skrappe begrænsninger på brugen af de forskellige servere, f.eks at der kun er en ad gangen der kan bruge servicen.

Links
Iperf relaterede links:

- [iperf commands](https://iperf.fr/iperf-servers.php)
- [public iperf server](https://fasterdata.es.net/performance-testing/network-troubleshooting-tools/iperf/)


___

**Besvarelse**
___instalation___
for at instalere skal men downloade en ZIP fil fra [iperf hjemmeside](https://iperf.fr/) som som understøtter ens OS. 
herefter skal den pakkes ud og så der den instaleret, dvs der er ikke nogle instalition som sådan. 

___ Kør Iperf mod en public server___
efter et par forsøg fandt jeg en server der var oppe, jeg ente ud med at bruge *speedtest.serverius.net* på PORT *5002* efter deres anbefalinger. 

serverne jeg ledte efter var også givet fra [iperf](https://iperf.fr/iperf-servers.php)

herefter kørte jeg komandoerne fra øvelsen:
> *iperf3.exe -c __speedtest.serverius.net__ -p 9201 -t 10 -i 1 -p __5002__*

> *iperf3.exe -c __speedtest.serverius.net__ -p 9201 -t 10 -i 1 -p __5002__ -R*


resultatet for testen gav mig understådene output:

```

C:\****\****\*****\iperf-3.1.3-win64\iperf-3.1.3-win64>iperf3.exe -c speedtest.serverius.net -p 9201 -t 10 -i 1 -p 5002
Connecting to host speedtest.serverius.net, port 5002
[  4] local 192.168.87.162 port 63907 connected to 178.21.16.76 port 5002
[ ID] Interval           Transfer     Bandwidth
[  4]   0.00-1.00   sec  7.88 MBytes  66.0 Mbits/sec
[  4]   1.00-2.00   sec  8.75 MBytes  73.4 Mbits/sec
[  4]   2.00-3.00   sec  8.75 MBytes  73.4 Mbits/sec
[  4]   3.00-4.01   sec  8.38 MBytes  69.9 Mbits/sec
[  4]   4.01-5.01   sec  8.88 MBytes  74.5 Mbits/sec
[  4]   5.01-6.00   sec  10.0 MBytes  84.0 Mbits/sec
[  4]   6.00-7.00   sec  9.38 MBytes  78.8 Mbits/sec
[  4]   7.00-8.01   sec  9.88 MBytes  82.5 Mbits/sec
[  4]   8.01-9.00   sec  9.50 MBytes  80.2 Mbits/sec
[  4]   9.00-10.01  sec  9.38 MBytes  78.0 Mbits/sec
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth
[  4]   0.00-10.01  sec  90.8 MBytes  76.1 Mbits/sec                  sender
[  4]   0.00-10.01  sec  90.8 MBytes  76.1 Mbits/sec                  receiver

iperf Done.

```


___modsat retning :___ 


```
C:\Users\tjs_1\Downloads\iperf-3.1.3-win64\iperf-3.1.3-win64>iperf3.exe -c speedtest.serverius.net -p 9201 -t 10 -i 1 -p 5002 -R
Connecting to host speedtest.serverius.net, port 5002
Reverse mode, remote host speedtest.serverius.net is sending
[  4] local 192.168.87.162 port 65123 connected to 178.21.16.76 port 5002
[ ID] Interval           Transfer     Bandwidth
[  4]   0.00-1.00   sec  5.30 MBytes  44.4 Mbits/sec
[  4]   1.00-2.01   sec  4.41 MBytes  36.8 Mbits/sec
[  4]   2.01-3.00   sec  5.15 MBytes  43.4 Mbits/sec
[  4]   3.00-4.01   sec  4.86 MBytes  40.5 Mbits/sec
[  4]   4.01-5.01   sec  6.17 MBytes  51.8 Mbits/sec
[  4]   5.01-6.00   sec  5.03 MBytes  42.5 Mbits/sec
[  4]   6.00-7.01   sec  4.64 MBytes  38.6 Mbits/sec
[  4]   7.01-8.00   sec  5.55 MBytes  47.0 Mbits/sec
[  4]   8.00-9.01   sec  6.95 MBytes  58.2 Mbits/sec
[  4]   9.01-10.00  sec  6.46 MBytes  54.3 Mbits/sec
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  56.0 MBytes  47.0 Mbits/sec   91             sender
[  4]   0.00-10.00  sec  54.8 MBytes  45.9 Mbits/sec                  receiver

iperf Done.
```

___ Konklution ___

>jeg undre mig over min hastighed, men jeg kan ikke helt se hvorfor forskællen mellem __ipfer__ og __speedtest__ er så stor. dette skal helt klart undersøges 
nærmere når tiden er til det. 
___

#Opgave Ekstra - Lav et netværks diagram

##Instruktioner
Genskab det nætværksdiagram, som underviseren lavede på tavlen under undervisningen,
 og beskriv betydningen af de forskellige elementer i diagrammet.
__udbytte__ 
*Opgaven vil kræve forståelse af netværksdiagrammer og netværksinfrastruktur.*

___

**Besvarelse**


![4/4](images/EkstraNetDir.png)


___

#Opgave Ekstra Setup

##Information
Iperf er et værktøj til den slags.

___

**Besvarelse**

Opgaver omkring installation af VMware og en Kali Live er ikke beskrevet, idet jeg allerede havde det preinstalleret. Men øvelsesvejledningerne beskriver fint, hvordan man kan komme igennem de forhindringer, der nu skulle komme undervejs

links til dette:

- [installer vmware](https://moozer.gitlab.io/course-networking-security/Bonus/install_vmware/)

- [installer kali live](https://moozer.gitlab.io/course-networking-security/Bonus/kali_on_vmware/)

*Problemer*

I forbindelse med en øvelse om installation af en .ova-fil og adgang til en router, stødte jeg på en række udfordringer, som jeg ønsker at dokumentere og reflektere over i denne rapport.

Efter at have læst øvelsesvejledningen, var min forventning, at der skulle laves en række konfigurationer på .ova-filen for at sikre adgang til routeren. 
Desværre oplevede jeg en vis forvirring omkring konto og adgangskode til routeren, hvilket gjorde opgaven mere kompleks end jeg havde forventet.

Efter at have undersøgt problemet nærmere og drøftet det med mine teammedlemmer, opdagede jeg, at opgaven var mere simpel end jeg havde troet. Faktisk var det
en "plug and play"-løsning, hvor installationen af .ova-filen var tilstrækkelig til at opnå adgang til routeren. 

**Løsning**

Jeg har tidligere haft erfaring med opsætning af VMWare-netværk, og denne erfaring har vist sig at være meget værdifuld. Da jeg blev bedt om at opsætte et VMWare-netværk i forbindelse med en øvelse, var jeg i stand til at gøre dette på kort tid og
få alt op og køre.

Jeg kunne trække på min tidligere erfaring med opsætning af netværk og var derfor i stand til at undgå mange af de udfordringer, som nybegyndere ofte står overfor. Desuden havde jeg tidligere gennemført en øvelse med opsætning af netværk,
og dette gav mig en god forståelse af, hvordan VMWare-netværk fungerer. 

dokumentationen kan ses her [Kapittel 6. assinment 10](https://gitlab.com/tjse28878/networking-basics/-/tree/master/Assignment%2010) 

For at opfylde kravene i opgaven opsatte jeg to VMnets med IP-adresserne 192.168.111.0/24 og 192.168.112.0/24. Jeg valgte at bruge VMnets 12 og 13, da dette ville mindske forvirringen i opsætningen af netværket.

Jeg havde allerede en Kali installeret, og jeg lavede to kloner af den. Derefter gik jeg ind i deres adapterindstillinger og valgte at Kali 1 skulle køre på VMnet 11, og Kali 2 skulle
køre på VMnet 12. Jeg tilføjede derefter i routerens adapterindstillinger, hvilket netværk de skulle være forbundet til, nemlig de tidligere nævnte VMnet 11 og 12, men jeg tilføjede også et
NAT-netværk, som ligger som standard på VMNet 8.

Herefter startede jeg Kali's og testede forbindelsen ved at udføre nogle tests for at se, om der var forbindelse. Et udsnit af mine resultater kan ses i kodeboksen nedenfor.



```
┌──(kali㉿kali)-[~]
└─$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.112.101  netmask 255.255.255.0  broadcast 192.168.112.255
        inet6 fe80::4dbd:77a4:35e1:9a2c  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:e7:ac:79  txqueuelen 1000  (Ethernet)
        RX packets 54  bytes 6236 (6.0 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 168  bytes 14308 (13.9 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 7  bytes 576 (576.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 7  bytes 576 (576.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

                                                                                                                                                            
┌──(kali㉿kali)-[~]
└─$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=17.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=16.0 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 16.023/16.719/17.415/0.696 ms
                                                                                                                                                            
┌──(kali㉿kali)-[~]
└─$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
192.168.112.2            ether   00:0c:29:5d:ae:47   C                     eth0
192.168.112.1                    (incomplete)                              eth0
                                                                                                                                                            
┌──(kali㉿kali)-[~]
└─$ ping 192.168.111.101 -c 3
PING 192.168.111.101 (192.168.111.101) 56(84) bytes of data.
64 bytes from 192.168.111.101: icmp_seq=1 ttl=63 time=2.32 ms
64 bytes from 192.168.111.101: icmp_seq=2 ttl=63 time=1.94 ms
64 bytes from 192.168.111.101: icmp_seq=3 ttl=63 time=1.33 ms

--- 192.168.111.101 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 1.329/1.863/2.324/0.409 ms
                                                     
```

Disse tests viser, at de to virtuelle maskiner, Kali 1 og Kali 2, er korrekt konfigureret og forbundet til deres respektive virtuelle netværk (VMnet 11 og VMnet 12).

Resultatet af ifconfig-kommandoen viser, at netværksadapteren for Kali 2 har en IP-adresse på 192.168.112.101 med en netværksmaske på 255.255.255.0. Dette er den forventede IP-adresse og netværksmaske, da Kali 2 er konfigureret til at køre på VMnet 12, som har et IP-adresseområde på 192.168.112.0/24.

Ping-testen til 8.8.8.8 viser, at Kali 2 har adgang til internettet.

ARP-kommandoen viser, at Kali 2 har opdaget en anden enhed med IP-adressen 192.168.112.2 på netværket. Dette kan være en router eller en anden virtuel maskine på VMnet 12.

Endelig viser ping-testen til 192.168.111.101, at der er en forbindelse mellem Kali 2 og en anden enhed med IP-adressen 192.168.111.101 på VMnet 11. Dette bekræfter, at de to virtuelle netværk er korrekt opsat og kan kommunikere med hinanden.

___

## Opgave - Bygge netværk

Du skal oprette et minimalt netværk ved hjælp af en Debian headless server og en openbsd router.
Du skal udføre opgaverne IPAM og netflow for at praktisk anvende de emner, vi har gennemgået i denne uge.

## Instruktioner

Ugens emner er IP-adresser, routere og VLANs. Målet er at lære at bygge et netværk, 
IPAM og netflow. Læringsmålene inkluderer at kunne designe, konstruere og implementere et  netværk
samt overvåge og administrere dets komponenter.


## Besvarelse

For at installere en minimal Debian headless server i VMware

1. Download Debian netinstall image fra [Debian's hjemmeside.](https://www.debian.org/CD/netinst/) Du kan vælge at downloade enten 32-bit eller 64-bit afhængigt af VMware-Geust, du vil installere.

2. Opret en ny virtuel maskine i VMware med mindst 512 MB RAM, 10 GB harddiskplads og ingen CD / DVD-drev.

3. Start den nye virtuelle maskine og vælg "Installer" fra opstartsmenuen.

4. Vælg det sprog og land, du ønsker at installere Debian på.

5. Konfigurer dit netværk ved at vælge en af de tilgængelige netværksenheder, som VMware tilbyder, såsom NAT eller Bridged. Konfigurer eventuelt en statisk IP-adresse.

6. Opret en ny brugerkonto og adgangskode, når du bliver bedt om det.

7. Vælg "Standard systemværktøjer" når du bliver bedt om at vælge pakker til installation.

8. Fortsæt installationen og vent på, at den er færdig.


Jeg har oplevet problemer med at oprette en SSH-forbindelse via Putty,
men jeg har ikke haft nogen problemer med at oprette en SSH-forbindelse fra PowerShell.
Jeg har forsøgt at ændre indstillingerne i Putty, men uden held. Jeg vil undersøge dette yderligere
senere og se, om jeg kan finde ud af, hvorfor SSH-forbindelsen ikke fungerer korrekt fra Putty,
og hvad der kan gøres for at løse problemet. I mellemtiden vil jeg arbejde videre med at bruge PowerShell
til at oprette SSH-forbindelser, da det fungerer uden problemer.

 så var det problem løst 
 ___


 __instalation af NetBox__



 __instaler Ntopng__ 


Jeg fulgte en trin-for-trin-vejledning for at installere [Ntopng](https://linuxhostsupport.com/blog/how-to-install-ntopng-on-debian-11/). Til at starte med havde jeg ikke sudo installeret på min brugerkonto,
som er en måde at give tilladelse til en bruger til at udføre administrative opgaver. 
For at undgå at skulle logge ind som root- eller sudo-bruger for at installere programmet,
valgte jeg at installere sudo på min brugerkonto. Dette gjorde, at jeg kunne følge
[installationsguiden](https://linuxconfig.org/rhel7-user-is-not-in-the-sudoers-file-error) uden at skulle bruge root- eller sudo-brugeren.


Jeg stødte på mange problemer,
da jeg prøvede at følge tidligere links til at opsætte Ntopng.
Jeg modtog mange fejl under min Wget-anmodning. Derfor besluttede jeg at undersøge,
hvad der kunne være galt. Jeg fandt ud af, at linket ikke var opdateret og besluttede
derfor at slette alle de downloads, jeg havde hentet. Derefter fulgte jeg en anden [vejledning](https://www.vultr.com/docs/how-to-install-ntopng-on-ubuntu-20-04/), som fungerede efter hensigten.

Jeg undersøger nu, hvad Ntopng er for en faktor, og hvad jeg kan bruge det til.

# Opgave 9/10 - Protokoller & Trafikanalyse

## Information

Protokoller er udbredte indenfor netværk, nogle af dem implementerer sikkerhed, nogle af dem gør ikke.
Formålet med denne opgave er at træne brugen af wireshark til at genkende protokoller.

__Instruktioner__

- Find to-tre protokoller fra wiresharks test pakker som du kan genkende
- Forstå hvad der foregår, evt. vha. google
- Tag noter, og vis det næste gang

__Besvarelse__

Jeg har valgt protokollerne DNS, TCP og TLSv1.3.
___

![](images/DNS_WS.png)


__DNS__ (Domain Name System) er en protokol, der bruges til at oversætte webadresser, 
som mennesker kan læse, til numeriske IP-adresser, som computere kan forstå.

Når du indtaster en webadresse i din browser,
sender din computer en DNS-anmodning til en DNS-server
for at få oplysninger om den numeriske IP-adresse, der svarer
til den webadresse, du vil besøge. DNS-serveren svarer
tilbage med den relevante IP-adresse,
og din computer kan derefter oprette forbindelse til webstedet.

___

![](images/TCP_TLS_WS.png)
__TLS:__ Hello client og server er en del af TLSv1.3-protokollen, som er en krypteringsprotokol, der bruges til at sikre kommunikationen mellem en klient og en server over et netværk, f.eks. internet.

Hello client og server refererer til de første trin i TLSv1.3-handshakeprocessen,
hvor klienten sender en "ClientHello" med oplysninger om de krypteringsprotokoller, 
den understøtter, og andre oplysninger. Serveren svarer med en "ServerHello", 
der vælger en krypteringsprotokol og sender certifikatet, som klienten kan verificere,
og derefter fortsætter de med at udveksle nøgler og krypteringsoplysninger
for at etablere en sikker forbindelse.

__TCP__ handshake er den proces, der finder sted, når to enheder ønsker at oprette forbindelse til hinanden
via TCP-protokollen.

TCP-handshaket begynder, når en enhed, som ønsker at oprette forbindelse 
(klienten), sender en anmodning til en anden enhed (serveren),
som er vært for den ønskede tjeneste.

Før data kan udveksles mellem klienten og serveren, skal de to enheder etablere en pålidelig
forbindelse, der giver sikker overførsel af data. Dette sker gennem en tre-trins handshake-proces,
hvor klienten sender en SYN-anmodning (SYN=1) til serveren, som svarer med en SYN-ACK-pakke (SYN=1, ACK=1),
og til sidst sender klienten en ACK-pakke (ACK=1) som bekræftelse på modtagelsen af serverens SYN-ACK-pakke.
___
# SSL dump opgave


Jeg har valgt at indhente et udsnit af TCPDump og viser hermed en lille
en analyserende over nogle af mine observationer. 
Det pågældende udsnit er tilgængeligt i følgende kodeboks.

De første to blokke med data repræsenterer et ClientHello-håndtryk,
hvilket indikerer en forespørgsel fra klienten til serveren for at etablere
en TLS-session. De to blokke er identiske bortset fra forbindelsesnummeret og tidsstemplerne.

Den tredje blok repræsenterer ServerHello, som er serverens svar på ClientHello.
Dette håndtryk indeholder information om serveren og TLS-sessionen, såsom krypteringsalgoritmer
og certifikater. Her ses også ja3s_str, som er en streng, der repræsenterer den krypteringsalgoritme 
og andre TLS-egenskaber, som serveren har valgt at bruge i denne session. ja3s_fp er en hashværdi
beregnet ud fra ja3s_str, som kan bruges til at identificere den specifikke TLS-konfiguration,
der blev brugt i sessionen.

Den fjerde blok repræsenterer en ChangeCipherSpec-meddelelse, som indikerer, at både klienten og serveren nu vil
begynde at bruge den aftalte krypteringsalgoritme til at kryptere og dekryptere data i denne session.

Samlet set viser disse data, at en TLS-session blev oprettet mellem en klient og server,
og at der blev brugt en specifik krypteringsalgoritme til at sikre kommunikationen mellem dem. 
Derudover blev der også genereret en hashværdi til at identificere den specifikke TLS-konfiguration,
der blev brugt i sessionen.



```
{
  "connection_number": 29,
  "record_count": 1,
  "timestamp": "1678656361.7999",
  "src_name": "192.168.137.138",
  "src_ip": "192.168.137.138",
  "src_port": 47336,
  "dst_name": "172.217.18.3",
  "dst_ip": "172.217.18.3",
  "dst_port": 443,
  "record_len": 512,
  "record_ver": "3.1",
  "msg_type": "Handshake",
  "handshake_type": "ClientHello",
  "ja3_str": "771,4865-4867-4866-49195-49199-52393-52392-49196-49200-49162-49161-49171-49172-156-157-47-53,
  0-23-65281-10-11-35-16-5-34-51-43-13-45-28-21,29-23-24-25-256-257,0",                                                                                                                                        
  "ja3_fp": "579ccef312d18482fc42e2b822ca2430"
}
{
  "connection_number": 30,
  "record_count": 1,
  "timestamp": "1678656361.8136",
  "src_name": "192.168.137.138",
  "src_ip": "192.168.137.138",
  "src_port": 47344,
  "dst_name": "172.217.18.3",
  "dst_ip": "172.217.18.3",
  "dst_port": 443,
  "record_len": 512,
  "record_ver": "3.1",
  "msg_type": "Handshake",
  "handshake_type": "ClientHello",
  "ja3_str": "771,4865-4867-4866-49195-49199-52393-52392-49196-49200-49162-49161-49171-49172-156-157-47-53,
  0-23-65281-10-11-35-16-5-34-51-43-13-45-28-21,29-23-24-25-256-257,0",                                                                                                                                        
  "ja3_fp": "579ccef312d18482fc42e2b822ca2430"
}
{
  "connection_number": 29,
  "record_count": 2,
  "timestamp": "1678656361.8482",
  "src_name": "fonts.gstatic.com",
  "src_ip": "172.217.18.3",
  "src_port": 443,
  "dst_name": "192.168.137.138",
  "dst_ip": "192.168.137.138",
  "dst_port": 47336,
  "record_len": 122,
  "record_ver": "3.3",
  "msg_type": "Handshake",
  "handshake_type": "ServerHello",
  "ja3s_str": "771,4865,51-43",
  "ja3s_fp": "eb1d94daa7e0344597e756a1fb6e7054"
}
{
  "connection_number": 29,
  "record_count": 3,
  "timestamp": "1678656361.8482",
  "src_name": "fonts.gstatic.com",
  "src_ip": "172.217.18.3",
  "src_port": 443,
  "dst_name": "192.168.137.138",
  "dst_ip": "192.168.137.138",
  "dst_port": 47336,
  "record_len": 1,
  "record_ver": "3.3",
  "msg_type": "ChangeCipherSpec"
  ```

For at slå domainnavne fra i SSL-analyse med ssldump kan du bruge flaget -N,
som vil erstatte navnene med IP-adresser. Du kan køre ssldump fra kommandolinjen med følgende kommando:

```ssldump -j -ANH -n -N -i any | jq . ```

Dette vil deaktivere DNS-opslag og i stedet vise IP-adresser for kilder og mål.

___
 