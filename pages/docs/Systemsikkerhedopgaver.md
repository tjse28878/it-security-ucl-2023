# **Opgave 1 Læringsmål** 

## Viden
** Generelle governance principper / sikkerhedsprocedurer**
-   Ramme værker, for at skabe noget regler for at kunne sikre sit system, (alt papirarbejdet)

** Væsentlige forensic processer** 
   - Måder man kan lave data analyser og efterforsking, for at finde frem til eventuelle begået kriminilitet

**Relevante it-trusler**
   - Forskellige former for it krimilitet, eks. Malware, CaaS osv. 

** Relevante sikkerhedsprincipper til systemsikkerhed**
   -    Måder at kunne sikre sit system

OS roller ift. sikkerhedsovervejelser
- Forskellige former for os systemer + overvejelser, også forskellige roller som skal have adgang til at kunne se de forskellige ting.

Sikkerhedsadministration i DBMS
- Administrationen af database systemer

* Færdigheder 
** Udnytte modforanstaltninger til sikring af systemer
- Måder at kunne sikre sit system på, eks. igennem en firewall 

** Følge et benchmark til at sikre opsætning af enhederne
- Kunne følge en guide til opsætning af enheder

** Implementere systematisk logning og monitering af enheder
- Intrusion detection, prevention detection. (WireShark)

** Analysere logs for incidents og følge et revisionsspor
- Man skal se efter om der eventuelt været nogen inceidents, og kunne fortælle om hvordan det eventuelt kunne være sket

** Kan genoprette systemer efter en hændelse.
- Håndteringen af genoprettelse af systemer 

** Kompetancer
** håndtere enheder på command line-niveau**


- Kunne styre enheder fra en commando linje, unden at se en gui

**håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
- Man kan bruge forskellige værktøjer som eks. wireshark og burpsuite til at finde frem til forskellige typer af trusler 

**Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser.
- Finde frem til den bedste løsning på problemerne
**håndtere relevante krypteringstiltag
- Kunne håndtere hvad for nogen data som skal krypteres, eventuelt i en database.

# Opgave 2 

##Information

Formålet med denne opgave er at introducerer de grundlæggende linux kommandoer.

I opgaven skal der eksperimenteres med Linux CLI kommandoer i BASH shell. Fremgangsmåden er at du bliver bedt om at eksekverer en kommando, og herefter noter hvad der sker. Målet med disse øvelser er at du skal bygge et Cheat sheet med linux kommandoer i dit gitlab repositorier, og få en genral rutinering med grundlæggende Linux Bash kommandoer. Det betyder følgende for alle trin i opgaven:

- Udfør kommandoen.
- Observer resultatet, og noter det herefter i dit Cheat sheet Altså efter hver eksekveret kommando, skal du kunne redegøre for hvad den gjorde

Resterene information af opgaven kan findes i [Opgavebeskrivelsen](https://ucl-pba-its.gitlab.io/exercises/system/3_Navigation_i_Linux_filesystem_med_bash/)

Resultatet af denne opgave kan ses i **Linux Cheat sheet**