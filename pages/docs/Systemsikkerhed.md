# Systemsikkerhed

## Læringsmål
**Viden**

*Den studerende har viden om:*

- Generelle governance principper / sikkerhedsprocedurer
- Væsentlige forensic processer
- Relevante it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- OS roller ift. sikkerhedsovervejelser
- Sikkerhedsadministration i DBMS.

**Færdigheder**

*Den studerende kan:*

- Udnytte modforanstaltninger til sikring af systemer
- Følge et benchmark til at sikre opsætning af enhederne
- Implementere systematisk logning og monitering af enheder
- Analysere logs for incidents og følge et revisionsspor
- Kan genoprette systemer efter en hændelse.

**Kompetencer**

*Den studerende kan:*

- Håndtere enheder på command line-niveau
- Håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
- Håndtere udvælgelse, anvendelse og implementering af
- Praktiske mekanismer til at forhindre, detektere og reagere over for specifikke
- IT-sikkerhedsmæssige hændelser.
- Håndtere relevante krypteringstiltag

## Eksamen
**ECTS:**

Prøvens omfang er 10 ECTS.

**Prøveform:**

Prøven består af en skriftlig aflevering (individuelt eller i en gruppe med op til tre studerende) fulgt af en individuel mundtlig eksamen. 

Den skriftlige aflevering må maksimum være 10 normalsider foruden bilag. Ved to studerende er det maksimale antal 15 normalsider. Ved tre studerende er det maksimale antal 20 normalsider. 

En normalside er 2.400 tegn inkl. mellemrum og fodnoter. Forside, indholdsfortegnelse, litteraturliste samt bilag tæller ikke med heri. Bilag er uden for bedømmelse og kan ikke forventes læst. 

For nærmere detaljer se afsnittet om ”Krav til skriftlige prøver og projekter”. Den mundtlig prøve er individuel og varer 25 minutters inkl. votering

**Bedømmelsen:**

Prøven bedømmes efter 7-trinsskalaen ved en intern bedømmelse. I henhold til Eksamensbekendtgørelsen foretages der altid en individuel bedømmelse. 
Bedømmelsen er en helhedsvurdering af den skriftlige og mundtlige præstation. Der er ikke krav til individualisering af det skriftlige gruppearbejde

**Prøven er ikke bestået:**

Prøveformen er den samme ved reeksamen

Det er muligt at indlevere en ny skriftlig opgave.
