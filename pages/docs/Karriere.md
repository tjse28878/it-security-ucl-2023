# Skab din egen karrierevej - find ønskejobbet

I dette fag får du mulighed for at opbygge viden og kompetencer indenfor det fagrelaterede emne du interesserer dig for - og som du gerne vil arbejde med i fremtiden.

I dette fag får du skærpet dit fokus på dine kompetencer og får skabt et klarere overblik over din karriere de næste 4-5 år frem i tiden.

Områder vi tager fat på:

- Overvinde personlige barrierer
- Skabe struktur i dit kommende arbejdsliv
- Få øje på muligheder og udfordringer
- Indblik i moderne ledelsesstil og organisationsformer
- Do’s and Don’t i forhold til din egen karriereplanlægning
 
….og til slut vil du være klar til at søge efter relevante virksomheder der matcher dine ambitioner.

Forløbet er designet over Design thinking modellen – du skal lave en ”prototype” på dit drømmejob - og vejen derhen. 
Du tester din plan og laver forbedringer på – indtil du står med det resultat  du helst vil have – et fuldt overblik over de næste skridt i din karriere, og konkrete tiltag til hvordan du kommer videre efter endt studie.

Undervisningen er tilrettelagt så den veksler mellem oplæg på klassen fra underviseren og dit arbejde med din personlige udviklingsplan – den sidste del foregår på klassen, hvor I giver sparring og input til hinanden i studiegrupper.

## Læringsmål

Læringsudbyttet omfatter den viden, de færdigheder og de kompetencer, du vil opnå fra
valgfaget.

**Viden**

*Den studerende har viden og forståelse for:*

- Praksis og metoder/teorier relateret til indholdet af valgfaget
- Etablering og udnyttelse af viden ved brug af egne netværk relateret til valgfaget
 
**Færdigheder**

- Anvende centrale metoder og værktøjer relateret til valgfaget 
- Vurder praktiske problemstillinger samt begrunde og udvælge passende modeller.
- Evaluere praksis-relaterede emner problemstillinger og udvælge og formidle passende løsninger.
- Evaluere, udarbejde og vælge løsninger til de relevante emner relateret til valgfaget.

**Kompetencer**

*Den studerende er i stand til at:*

- Håndtere og administrere udfordringer relateret til valgfaget
- Deltage som rådgiver relateret til emner fra valgfaget
- Udvikle egen praksis/karriere i en struktureret kontekst.

## Eksamen

Som afslutning på forløbet skal du lave en femsiders rapport – den laves individuelt, og skal indeholde:

- Kort resume
- Personlig T-model med kommentarer
- Personligt kompas – med kommentarer
- Personlig Odysee – med kommentarer
- Fire dimensional læring – 21 århundred færdigheder og kompetencer – begrund hvilke tre elementer der er de mest relevante for netop dig og din professionelle udvikling
- Relater og diskuter din fremtidige karriereplan samt din professionelle og personlige udvikling baseret på det ovennævnte og skab en plan for din praktik med relevante fokuspunkter.

Rapporten afleveres via Wiseflow og bedømmes efter 7-trins skala med intern bedømmelse.